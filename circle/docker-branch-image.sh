#!/bin/bash -e

set -e;
set -o pipefail

get_script_dir() {
     SOURCE="${BASH_SOURCE[0]}"
     # While $SOURCE is a symlink, resolve it
     while [ -h "$SOURCE" ]; do
          DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
          SOURCE="$( readlink "$SOURCE" )"
          # If $SOURCE was a relative symlink (so no "/" as prefix, need to resolve it relative to the symlink base directory
          [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
     done
     SOURCEDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
}

get_script_dir
source $SOURCEDIR/common.sh

#TODO Refactor needed: to support other build system like CodeBuild and bitbucket pipeline we need refactor how we read these variables (with a set of nested if)
export DOCKERFILE=${DOCKERFILE:-Dockerfile}
export GIT_BRANCH=${CIRCLE_BRANCH}
export GIT_SHA1=${CIRCLE_SHA1}
export BUILD_NUM=${CIRCLE_BUILD_NUM}

export DOCKER_TAG=${GIT_BRANCH////_} # Strip '/' char
export IMAGE_BUILD_DIR=${IMAGE_BUILD_DIR:-"."}

if [[ -n $DOCKER_PASS ]]; then
  docker_login || exit 1
  docker_build_and_push $DOCKER_PROJECT/$IMAGE_NAME:$DOCKER_TAG $IMAGE_BUILD_DIR || exit 1
fi

if [[ -n $AWS_SECRET_ACCESS_KEY ]]; then
  aws_ecr_login || exit 1
  docker_build_and_push $AWS_ECR_URI:$DOCKER_TAG $IMAGE_BUILD_DIR || exit 1
fi
