log() {
  echo -e "$(date "+%T.%2N") ${@}"
}

info() {
  log "INFO  ==> ${@}"
}

warn() {
  log "WARN  ==> ${@}"
}

error() {
  log "ERROR ==> ${@}"
}

##############
# Common sanity checks
##############
if [[ -z $DOCKER_PROJECT && -z $QUAY_PROJECT && -z $GCLOUD_PROJECT && -z $AWS_ECR_URI ]]; then
  error "You must specify at least one project"
  exit 1
fi

##############
# DockerHub
##############

docker_login() {
  local username=$DOCKER_USER
  local password=$DOCKER_PASS
  local email=$DOCKER_EMAIL
  local registry=${1}
  case "$1" in
    quay.io )
      username=$QUAY_USER
      password=$QUAY_PASS
      email=$QUAY_EMAIL
      ;;
  esac
  info "Authenticating with Docker Hub..."
  docker login -e $email -u $username -p $password $registry
}

docker_build() {
  local IMAGE_BUILD_TAG=${1}
  local IMAGE_BUILD_DIR=${2:-.}
  local IMAGE_BUILD_ORIGIN=${3}

  if [[ -n $IMAGE_BUILD_ORIGIN ]]; then
    echo "ENV BITNAMI_CONTAINER_ORIGIN=$IMAGE_BUILD_ORIGIN" >> $IMAGE_BUILD_DIR/$DOCKERFILE
  fi

  info "Building '${IMAGE_BUILD_TAG}'..."
  if [[ ! -f $IMAGE_BUILD_DIR/$DOCKERFILE ]]; then
    error "$IMAGE_BUILD_DIR/$DOCKERFILE does not exist, please inspect the release configuration in circle.yml"
    return 1
  fi

  docker build --label=com.addictive.circle_build_num=${BUILD_NUM} --label=com.addictive.git_commit=${GIT_SHA1} \
  --label=com.addictive.git_branch=${GIT_BRANCH} --rm=false -f $IMAGE_BUILD_DIR/$DOCKERFILE -t $IMAGE_BUILD_TAG $IMAGE_BUILD_DIR || return 1
}

docker_push() {
  local IMAGE_BUILD_TAG=${1}

  info "Pushing '${IMAGE_BUILD_TAG}'..."
  docker push $IMAGE_BUILD_TAG
}

docker_build_and_push() {
  if ! docker_build ${1} ${2} ${3}; then
    return 1
  fi
  docker_push ${1}
}

##############
# AWS
##############

aws_ecr_login() {
  info "Authenticating with AWS ECR..."
  aws ecr get-login --no-include-email| bash - || exit 1
}
