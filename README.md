Set of bash script to build, tag and push docker images on CircleCI.

To configure a project the first thing is to configure your `.circleci/config.yml`

```
version: 2

jobs:
  build:
    docker:
      #Primary image
      - image: addictive/minideb-cci
    working_directory: /working_directory

    environment:
      TEST_RESULTS: /tmp/test-results
      RELEASE_SERIES_LIST: 0
      LATEST_STABLE: 0
      AWS_DEFAULT_REGION: eu-west-1
      DOCKER_BUILD_PATH: "."
      DOCKERFILE: "docker/Dockerfile"
      DOCKER_NAMESPACE: 470031436598.dkr.ecr.eu-west-1.amazonaws.com
      DOCKER_REPO_NAME: meterlinq/api
      #BASH_ENV: "circleci_local_env"

    branches:
      only:
        - develop
        - feature/devops

    steps:
      - checkout

      - setup_remote_docker

      - run:
          name: Build
          command: |
            curl -sL https://bitbucket.org/pitchtarget/addictive-docker/get/master.tar.gz | tar --strip-components=1 -xzf - && circle/docker-branch-image.sh
```

`$DOCKER_NAMESPACE/$DOCKER_REPO_NAME:$GIT_BRANCH`

Parameter explanation:

* `setup_remote_docker` : Required to build Docker images for deployment, you must use a special setup_remote_docker key which creates a separate environment for each build for security. This environment is remote, fully-isolated and has been configured to execute Docker commands. [ref](https://circleci.com/docs/2.0/building-docker-images/).

The run step will execute

* `docker-release-image.sh` script when the build is triggered by a new git tag;
* `docker-branch-image.sh` when the build is a simple commit on a branch;

* `environment` : here you set all the ENV variable to configure our scripts.

Using variables you can fo

* `DOCKER_BUILD_PATH`: the `PATH` for the `docker build` command.
* `DOCKERFILE`: the path of the Dockerfile, it's relative to the `DOCKER_BUILD_PATH`

If you want to push the image on DockerHub, just add these variables

* `DOCKER_NAMESPACE`: on DockerHub you can have different accounts, each account has it's own namespace (ex: https://hub.docker.com/r/addictive/minideb-cci/ has namespace `addictive` and repo name `minideb-cci`)
* `DOCKER_REPO_NAME`:  the repository name
* `DOCKER_PASS` (WARNING: add these variable as an encrypted var, ex: CircleCI secrets)
* `DOCKER_USER`

Images will be tagged as:

*
* `$DOCKER_NAMESPACE/$DOCKER_REPO_NAME:$GIT_BRANCH` when the build is a simple commit on a branch;  the `GIT_BRANCH` is set read from CircleCI;

If you want to push on ECR you just need to add these variables:

* `AWS_SECRET_ACCESS_KEY` (WARNING: add these variable as an encrypted var, ex: CircleCI secrets)
* `AWS_ECR_URI`: ex: 470031436598.dkr.ecr.eu-west-1.amazonaws.com
* `AWS_REPO_NAME`: ex: pitchtarget/api
* `AWS_DEFAULT_REGION`: ex: eu-west-1

To find the values just check into your AWS ECS console or into the output of the Cloudformation stack for the `Repository URI` 470031436598.dkr.ecr.eu-west-1.amazonaws.com/pitchtarget/api

Images will be tagged as:

* `$AWS_PROJECT/$DOCKER_REPO_NAME:$GIT_BRANCH` when the build is a simple commit on a branch;  the `GIT_BRANCH` is set read from CircleCI;
*


cat circle.yml

```
machine:
  services:
  - docker
  environment:
    RELEASE_SERIES_LIST: 2.4,2.3,2.2,2.1
    LATEST_STABLE: 2.4
    DOCKER_REPO_NAME: ruby
    DOCKER_NAMESPACE: bitnami
    QUAY_PROJECT: bitnami
    GCLOUD_PROJECT: bitnami-containers

dependencies:
  override:
  - docker info
  - gcloud version
  - curl -sL https://raw.githubusercontent.com/bitnami/test-infra/master/circle/docker-pull-cache.sh | bash -

test:
  override:
  - curl -sL https://raw.githubusercontent.com/bitnami/test-infra/master/circle/docker-image-test.sh | bash -

deployment:
  development:
    branch: master
    commands:
    - curl -sL https://raw.githubusercontent.com/bitnami/test-infra/master/circle/docker-development-image.sh | bash -
  release:
    tag: /^[0-9].*-r[0-9]+$/
    commands:
    - curl -sL https://raw.githubusercontent.com/bitnami/test-infra/master/circle/docker-release-image.sh | bash -

```

# Refactor IDEA

Create a cmdline tool that makes some sanity checks on the Parameter


# TEST

The `test_app` directory contains a test Dockerfile and a stack_master config that we used to create a test ECR repository. To create or update the stack `aws-vault exec -n pt -- bundle exec stack_master apply eu-west-1 addictive_docker_test_app`

NOTE: To push to ECR you must test with a user belonging to the IAM group `EcrDeployGroup-addictive-docker-test-app`

To locally test the script `docker-branch-image.sh`, just run: `CIRCLE_SHA1=dsfasfasfds CIRCLE_BUILD_NUM=1001 AWS_ECR_URI=470031436598.dkr.ecr.eu-west-1.amazonaws.com/addictive/addictive_docker_test_app GIT_TAG=0.0.1 DOCKERFILE=test_app/Dockerfile aws-vault exec -n pt -- ./circle/docker-branch-image.sh`  

You can change GIT_TAG and GIT_BRANCH variables as you need.

To test the circlici config you need to push it to bitbucket because the command `curl -sL https://bitbucket.org/pitchtarget/addictive-docker/get/master.tar.gz | tar --strip-components=1 -xzf - && circle/docker-branch-image.sh` will be run within the containers; you can change the branch you are downloading.

NOTE: the circleci test is executed within a Docker container like in production.




These is the expected behavior; TAG should have the precedence on branches; CIRCLECI variables should have the precedence on GIT_* variables

```
DOCKER_NAMESPACE=1 GIT_BRANCH=2 ./circle/docker-branch-image.sh
19:01:27.76 INFO  ==> we are on a git branch
19:01:27.76 INFO  ==> Building tag 2

DOCKER_NAMESPACE=1 GIT_BRANCH=2 CIRCLE_BRANCH=3 ./circle/docker-branch-image.sh
19:01:47.76 INFO  ==> we are on a git branch
19:01:47.76 INFO  ==> Building tag 3

DOCKER_NAMESPACE=1 GIT_BRANCH=2 CIRCLE_BRANCH=3 CIRCLE_TAG=4 ./circle/docker-branch-image.sh
19:02:09.41 INFO  ==> we are on a git tag 4
19:02:09.41 INFO  ==> Building tag 4

DOCKER_NAMESPACE=1 GIT_BRANCH=2 CIRCLE_BRANCH=3 GIT_TAG=4 ./circle/docker-branch-image.sh
19:02:17.77 INFO  ==> we are on a git tag 4
19:02:17.78 INFO  ==> Building tag 4

DOCKER_NAMESPACE=1 GIT_BRANCH=2 CIRCLE_BRANCH=3 GIT_TAG=4 CIRCLE_TAG=5 ./circle/docker-branch-image.sh
19:03:16.31 INFO  ==> we are on a git tag
19:03:16.31 INFO  ==> Building tag 5
```
